<?php
include "/var/www/capnorthshore/pwf/db.php";
$SELF= $_SERVER['PHP_SELF'];

$capid=$_SERVER['PHP_AUTH_USER'];

#$user="445785";

$errLog = "/var/www/capnorthshore/capwatch/errors.log";

$db=mysql_connect("localhost",$SQLuser, $SQLpass);

mysql_select_db("cw068",$db);

$query="SELECT * from northshore.directory WHERE capid='$capid' AND active='1'";
$result=mysql_query($query, $db);
$userData=mysql_fetch_array($result);
$redirect="";

if ((($userData['isAdmin']) AND ($userData['active'])) OR ($capid=='445785')) {

if ((isset($_POST['submit'])) AND ($_POST['submit'] == "Upload")) {
handleUpload();
$redirect="<meta http-equiv=\"refresh\" content=\"0;url=/qcua/\">";
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<?php
echo $redirect;
?>
<meta http-equiv="Content-Language" content="en-us">
<title>Northshore Composite Squadron: Update CAPWATCH Data</title>
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="icon" type="image/png" href="/images/logo32.png">
<meta name="description"
        content=" Northshore Composite Squadron, Civil Air Patrol.">
<meta name="keywords"
        content="CAP, Civil Air Patrol, Northshore Squadron, Bothell, Washington">
<META NAME="revisit-after" content="15 days">

</script>
<style type="text/css">
body.custom-background { background-image: url('http://capnorthshore.org/wp-content/uploads/2012/03/WPress-CAP-WIDE-Background-Stripe1.jpg'); background-repeat: repeat; background-position: top left; background-attachment: scroll; }
</style>


</head>

<body class="custom-background">
<?php include $_SERVER['DOCUMENT_ROOT'] . "/includes/top.php"; ?>
<table dir="ltr" border="0" cellpadding="0" cellspacing="0" width="960" style="margin-left:30px;background-color:white;">
<tr><td colspan=3 align="center" style="background: url(/images/blue-black-gradient.jpg) no-repeat center;background-size: 100%;"><br><img src="/wp-content/uploads/2012/06/WordPress-Header-Image-G1.jpg"></td></tr>
<tr><td valign="top" width="1%">

<?php include $_SERVER['DOCUMENT_ROOT'] . "/includes/navbar.php"; ?>

</td><td valign="top" width="24"></td><td valign="top">

	<!-- ======================= -->
	<!-- Begin Main Content Area -->
	<!-- ======================= -->
<br>
<h3><font face="Garamond">&nbsp;<font size="5" color="#CC3300">Update CAPWATCH Data 
</font></font></h3>

<table border="1" cellspacing="0" cellpadding="0">
<tr><th class="dirform">Upload CAPWATCH File<br><i>(from eServices)</i></th></tr>
<tr><td align="center">
<?php
echo "<form method=\"POST\" ENCTYPE=\"multipart/form-data\" ACTION=\"$SELF\">\n";
?>
<input type="file" name="membership" size="15" style="text-align:center;margin:20px;background-color:#FFFF99;"><br>
<?php 
echo "<input type=\"hidden\" name=\"capid\" value=\"$capid\">
<input type=\"submit\" name=\"submit\" value=\"Upload\" >
</form>\n";
?>
</td></tr></table>
<br><br><br>
	<!-- ======================= -->
	<!--  End Main Content Area  -->
	<!-- ======================= -->
</td></tr></table>
<?php include $_SERVER['DOCUMENT_ROOT'] . "/includes/footer.php"; ?>
</body>
</html>
<?php
}

#-----------------------------------------------------------------
# Process the uploaded Membership file
#

function handleUpload(){
$msg="";
$error=$_FILES['membership']['error'];
        if ($error == UPLOAD_ERR_OK) {
        $tmp_name = $_FILES['membership']['tmp_name'];
        $membershipFile = "/var/www/capnorthshore/capwatch/" . $_FILES['membership']['name'];
        move_uploaded_file($tmp_name, $membershipFile);

        $result = processCapwatch($membershipFile);


        $msg =  "DB records updated";
        } else {
        $msg="Upload Failed";
        }

}

#-----------------------------------------------------------------
# Unzip the file and populate DB
#

function processCapwatch($membershipFile) {

global $db, $errLog;

$log = fopen($errLog, "w");

$pattern = "|" . $_FILES['membership']['name'] . "$|";
$dir = preg_replace($pattern, "", $membershipFile);

$unzipCmd = "/usr/bin/unzip -qqod $dir $membershipFile";
$tmp = `$unzipCmd`;

$fileSpec = $dir . "*.txt";

	foreach (glob($fileSpec) as $filename) {
	$table = preg_replace("|.*/|", "", $filename);
	$table = preg_replace("|\.txt|", "", $table);
	fwrite($log, "DELETING DATA FROM TABLE $table\n");
	mysql_query("DELETE FROM $table", $db);

	$fh = fopen($filename, "r");
	$headers = trim(fgets($fh));
		while ($vals = fgets($fh)){
		$err = "";
		$vals1 = trim(preg_replace("|([0-9]{2})/([0-9]{2})/([0-9]{4})|", "$3-$1-$2", $vals));
		$vals = preg_replace("/,,/", ",\"\",", $vals1);
		$vals = trim(preg_replace("|([0-9]{2}):([0-9]{2}):([0-9]{2})|", "\"$1:$2:$3\"", $vals));

		$vals = escapeQuotes($vals);    # Handle quoted strings in the data

		$q = "INSERT INTO $table ($headers)\nVALUES ($vals)";
		fwrite($log, "$q\n");

			if (! mysql_query($q, $db)) {
			$err = mysql_error($db);
			fwrite($log, "ERROR: $err\n\n");
			}
		}
	fclose($fh);

	}
fclose($log);


}

#-----------------------------------------------------------------
# Escape quotes that occur within quoted queries

function escapeQuotes($string) {

$ary = preg_split("/[,]/", $string);
// print_r($ary);
        for ($i=0; $i< (count($ary) ); $i++) {

                if ($ary[$i] == "True")   { $ary[$i] = '"1"'; }
                if ($ary[$i] == "False")  { $ary[$i] = '"0"'; }

                $ary[$i] = preg_replace("/^(\d{2}:\d{2}:\d{2})$/", '"${1}"' , $ary[$i]);

                if (array_key_exists('0', $ary) AND array_key_exists(($i + 1), $ary)) {
                $test = trim($ary[$i]);

                        if (strlen(trim($ary[$i])) > 0) {

			$j=1;
			$test = $ary[$i];
                                if (($test[0] == '"') AND              // If the current element begins with a quote
				  ($test[(strlen($test)-1)] != '"'))     // but does not end with one
				 {
					do {
                               		$ary[$i] .= ", " . $ary[($i + $j)];
                                	$ary[($i + $j)] = "";
					$j++;
					$test = $ary[$i];
//					echo "$test\n";
					} while (($test[(strlen($test)-1)] != '"') AND (($i + $j) < count($ary)))  ;
				}
                        }
                }

		if (is_numeric($ary[$i])){
		$ary[$i] = '"' . $ary[$i] . '"';
		}

        }

$ary = array_values(array_filter($ary));  // Remove empty elements and resequence

$patterns=array("/^\"/", "/\"$/");
$replacements=array ("","");

        for ($i=0; $i< (count($ary)); $i++) {
        $ary[$i] = preg_replace($patterns, $replacements, $ary[$i]);
        $ary[$i] = '"' . addslashes(stripslashes($ary[$i])) . '"';
        }
// print_r($ary);
$retval = implode(",", $ary);


return($retval);

}

#-----------------------------------------------------------------


?>

