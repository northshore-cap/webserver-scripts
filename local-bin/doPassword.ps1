<#
.SYNOPSIS
    Script to invoke CAP's CapWatch API
.DESCRIPTION
    THis script will invoke CapWatch's GET menthod and save the returned zipped data to a specified file path. In order to use this script, the user must pass in the ORGID (-org) and full filepath (-filePath) of where they would like to save the zip file.
    Additionally, the optional parameter unitonly (-unitOnly) can be set to 1 in order to pull data for the specific unit if the ORGID passed in is for a Group, Wing, or Region

    !!IMPORTANT!! The first time you run this script and any time you update your eServices password you need to encrypt your password. To encrypt your password, make sure you are in the working directory where the script is located, call the script passing in ONLY the -password parameter. See EXAMPLE 3 by typing "get-help [scriptLocation]\invokeCapWatch.ps1 -examples".
.NOTES
    Author     : Angel Lopez - Civil Air Patrol
.EXAMPLE
    The following example will call the CapWatch API requesting Alabama Wing Data and save the zip file to E:\CapWatchDownloads\ALWing.zip

    [scriptLocation]\InvokeCapWatch.ps1 -org 97 -filePath E:\CapWatchDownloads\ALWing.zip

    Expected Output: zip file containing 48 comma delimited flat files at E:\CapWatchDownloads\ALWing.zip
.EXAMPLE
    The following example will call the CapWatch API requesting Great Lakes Region's unit and save the zip file to E:\CapWatchDownloads\GLRUnit.zip

    [scriptLocation]\InvokeCapWatch.ps1 -org 177 -unitOnly 1 -filePath E:\CapWatchDownloads\GLRUnit.zip

    Expected Output: zip file containing 48 comma delimited flat files at E:\CapWatchDownloads\GLRUnit.zip

.EXAMPLE
    The following example will reset the encrypted password and generate an updated script file

    CD [scriptLocation]

    .\InvokeCapWatch.ps1 -password 'dummyPass'

    Expected Output: An updated InvokeCapWatch.ps1 file with an excrypted password at line number 82
#>

param(
[string] $org,
[string] $unitOnly = 0,
[string] $filePath,
[string] $password,
[string] $capid
)

function Reset-EncryptedPw ($capid,$password)
{
    [Byte[]] $key = (1..16)
    $password = $password | ConvertTo-SecureString -AsPlainText -Force
    $encryptedPass = $password | ConvertFrom-SecureString -Key $key
    $user = $capid
    $userPass = $encryptedPass
    $psCred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, ($userPass | ConvertTo-SecureString -Key $key)
    $pass = $psCred.GetNetworkCredential().Password
    $pair = "${user}:${pass}"
    $bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
    $base64 = [System.Convert]::ToBase64String($bytes)
    $basicAuthValue = "Basic $base64"
    $headers = @{ Authorization = $basicAuthValue }
echo $basicAuthValue
#$headers



#    exit
}

if ($password)
{
    Reset-EncryptedPw $capid $password
}

