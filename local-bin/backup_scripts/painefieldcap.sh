#!/bin/bash
#
# PAINEFIELDCAP BACKUP
#

REMOTE_HOST="capgrids.com"
RELATIVE_DIR="painefieldcap"
BASE_DIR="/var/www"
DAY=$(date +%A)
FN="painefieldcap-$DAY"
export GZIP=-9

/bin/rm -f $RELATIVE_DIR/backups/*.tgz
/usr/bin/mysqldump --opt painefield > $BASE_DIR/$RELATIVE_DIR/backups/painefield.sql
/usr/bin/mysqldump --opt painefield_wp > $BASE_DIR/$RELATIVE_DIR/backups/painefield_wp.sql
/usr/bin/gzip --force --best $BASE_DIR/$RELATIVE_DIR/backups/painefield.sql
/usr/bin/gzip --force --best $BASE_DIR/$RELATIVE_DIR/backups/painefield_wp.sql

cd $BASE_DIR
tar -czf /tmp/$FN.tgz $RELATIVE_DIR/htdocs $RELATIVE_DIR/bin $RELATIVE_DIR/backups/*.sql.gz $RELATIVE_DIR/lists  $RELATIVE_DIR/pwf

/bin/mv /tmp/$FN.tgz $BASE_DIR/$RELATIVE_DIR/backups
/usr/bin/rsync  --archive --perms --times --whole-file  --rsync-path="/usr/bin/rsync" --rsh="/usr/bin/ssh" $BASE_DIR/$RELATIVE_DIR/backups/*.tgz $REMOTE_HOST:/var/www/offsite_backups/


