#!/bin/bash
#
# OFLIGHTS BACKUP
#

REMOTE_HOST="capgrids.com"
RELATIVE_DIR="oflights"
BASE_DIR="/var/www"
DAY=$(date +%A)
FN="oflights-$DAY"
export GZIP=-9

/bin/rm -f $RELATIVE_DIR/backups/*.tgz
/usr/bin/mysqldump --opt oflights > $BASE_DIR/$RELATIVE_DIR/backups/oflights.sql
/usr/bin/gzip --force --best $BASE_DIR/$RELATIVE_DIR/backups/oflights.sql

cd $BASE_DIR
tar -czf /tmp/$FN.tgz $RELATIVE_DIR/htdocs $RELATIVE_DIR/bin $RELATIVE_DIR/backups/*.sql.gz

/bin/mv /tmp/$FN.tgz $BASE_DIR/$RELATIVE_DIR/backups
/usr/bin/rsync  --archive --perms --times --whole-file  --rsync-path="/usr/bin/rsync" --rsh="/usr/bin/ssh" $BASE_DIR/$RELATIVE_DIR/backups/*.tgz $REMOTE_HOST:/var/www/offsite_backups/


